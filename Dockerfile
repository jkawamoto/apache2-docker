# Constracting an Apache2 server which web data are stored data volume
FROM ubuntu:latest

MAINTAINER Junpei Kawamoto

# Installing Apache2
RUN echo "Installing Apache2"
RUN apt-get update
RUN apt-get -y install apache2 php5 libapache2-mod-php5

# Configuring Environment Variables
ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2

# Copying website definitions
RUN echo "Copying website definitions"
ADD ./conf/default.conf /etc/apache2/sites-available/default.conf
ADD ./conf/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf

RUN echo "Enabling websites and mods"
RUN a2ensite default
RUN a2ensite default-ssl
RUN a2enmod ssl
RUN a2enmod auth_digest

# Start Apache2
VOLUME /var/www/html
EXPOSE 80 443
CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]

